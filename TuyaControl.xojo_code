#tag Class
Protected Class TuyaControl
Inherits CurlSMultiMBS
	#tag Method, Flags = &h1
		Protected Function AddHeaders(curl as CURLSMBS) As Boolean
		  var id As String = Me.AccessID.Trim
		  If id.IsEmpty Then
		    Raise New RuntimeException(kerr_noAccessID, -2)
		  End If
		  var secret As String = Me.AccessSecret.Trim
		  If secret.IsEmpty Then
		    Raise New RuntimeException(kerr_noAccessSecret, -3)
		  End If
		  var i As Integer = DateTime.now.SecondsFrom1970 * 1000 // According to Python script.
		  var headers() As String 
		  headers.Add "client_id:" + id
		  var nonce As String = UUIDMBS.UUID.ValueHexString
		  var sign As String = id + i.ToString + StringToSign(curl, "GET") + "?grant_type=1"
		  sign = EncodeHex(Crypto.HMAC(secret, sign, Crypto.HashAlgorithms.SHA2_256)).Uppercase
		  System.DebugLog sign
		  headers.Add "sign:" + sign
		  headers.Add "t:" + i.ToString
		  headers.Add "nonce:" + nonce
		  headers.add "sign_method:HMAC-SHA256"
		  //Return True
		  // If returning here, result will be a time signature invalid error
		  curl.SetOptionHTTPHeader Array ("client_id", id, "sign", sign, "t", i.ToString, "sign_method", "HMAC-SHA256")
		  var ok As Boolean = curl.SetupOAuth(id, secret, "", "", "GET", EndpointURL, Array ("grant_type=1"), headers, Nil,nonce, i.ToString, "HMAC-SHA256")
		  var hs As String = curl.HeaderData
		  Return ok
		  // With Endpoint URL as it should be, returns no error but no result JSON as well.
		  // With curl.optionURL will return with a sign invalid error.
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  timer.CallLater 0, WeakAddressOf RaiseOpen
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub getToken()
		  var c As New CURLSMBS
		  c.CollectHeaders = True
		  c.CollectOutputData = true
		  c.OptionHeader = true
		  c.OptionURL = EndpointURL + tokenURL
		  c.OptionGet = True
		  If Me.AddHeaders(c) Then
		    If Me.AddCURL(c) Then
		      Me.Perform
		    End If
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub RaiseOpen()
		  RaiseEvent Opening
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function StringToSign(curl as CURLSMBS, method as String) As String
		  var content As String = EncodeHex(Crypto.SHA2_256(curl.InputData))
		  var s As String = method + linefeed + content + linefeed + linefeed + curl.OptionURL.Replace(EndpointURL, "")
		  // Should contain nonce too. Tried without before.
		  Return s
		End Function
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Opening()
	#tag EndHook


	#tag Property, Flags = &h0, Description = 54686520416363657373206F7220436C69656E74204944206E656365737361727920746F20616363657373205475796120436C6F7564
		AccessID As String
	#tag EndProperty

	#tag Property, Flags = &h0, Description = 54686520416363657373206F7220436C69656E74207365637265742F70617373776F7264206E656365737361727920746F20616363657373205475796120436C6F7564
		AccessSecret As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Endpoint As Endpoints
	#tag EndProperty

	#tag ComputedProperty, Flags = &h1
		#tag Getter
			Get
			  Select Case Endpoint
			  Case Endpoints.China
			    Return "https://openapi.tuyacn.com"
			  Case Endpoints.WesternAmerica
			    Return "https://openapi.tuyaus.com"
			  Case Endpoints.EasternAmerica
			    Return "https://openapi-ueaz.tuyaus.com"
			  Case Endpoints.CentralEurope
			    Return "https://openapi.tuyaeu.com"
			  Case Endpoints.WesternEurope
			    Return "https://openapi-weaz.tuyaeu.com"
			  Case Endpoints.India
			    Return "https://openapi.tuyain.com"
			  Else
			    Raise New RuntimeException(kerr_noEndpoint, -1)
			  End Select
			  
			End Get
		#tag EndGetter
		Protected EndpointURL As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return &u0a
			End Get
		#tag EndGetter
		Shared linefeed As String
	#tag EndComputedProperty


	#tag Constant, Name = kerr_noAccessID, Type = String, Dynamic = True, Default = \"No access ID", Scope = Public
		#Tag Instance, Platform = Any, Language = de, Definition  = \"Keine Zugangs-ID"
	#tag EndConstant

	#tag Constant, Name = kerr_noAccessSecret, Type = String, Dynamic = True, Default = \"No access Secret (password)", Scope = Public
		#Tag Instance, Platform = Any, Language = de, Definition  = \"Kein Zugangs-\xE2\x80\x9EGeheimnis\xE2\x80\x9C (Passwort)"
	#tag EndConstant

	#tag Constant, Name = kerr_noEndpoint, Type = String, Dynamic = True, Default = \"No endpoint selected", Scope = Public
		#Tag Instance, Platform = Any, Language = de, Definition  = \"Kein Endpoint ausgew\xC3\xA4hlt"
	#tag EndConstant

	#tag Constant, Name = tokenURL, Type = String, Dynamic = False, Default = \"/v1.0/token/", Scope = Public
	#tag EndConstant


	#tag Enum, Name = Endpoints, Type = Integer, Flags = &h0
		China
		  WesternAmerica
		  EasternAmerica
		  CentralEurope
		  WesternEurope
		India
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="AccessID"
			Visible=true
			Group="Behavior"
			InitialValue=""
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AccessSecret"
			Visible=true
			Group="Behavior"
			InitialValue=""
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Endpoint"
			Visible=true
			Group="Behavior"
			InitialValue=""
			Type="Endpoints"
			EditorType="Enum"
			#tag EnumValues
				"0 - China"
				"1 - WesternAmerica"
				"2 - EasternAmerica"
				"3 - CentralEurope"
				"4 - WesternEurope"
				"5 - India"
			#tag EndEnumValues
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
